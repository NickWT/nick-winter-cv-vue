# nick-winter

My intention for this project is to be an online CV outlining my professional skills and background in software development. I intend to continue this project for the forseeable future.

## To access this project please go to the following URL
```
www.nick-winter.com
```

## Mobile Friendly
```
Try it on your mobile device!
```

Any feedback is greatly appreciated :D