import { shallowMount, createLocalVue } from '@vue/test-utils';
import CvAbout from '../views/cvAbout.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('cvAbout.vue', () => {
  let wrapper;
  let checkAuth = jest.fn();
  let downloadCV = jest.fn();
  let saveCV = jest.fn();
  let convertBuffer = jest.fn();
  let simulateLoading = jest.fn();
  let scrollTop = jest.fn();

  let state;
  let store;

  beforeEach(() => {
    state = {
      is: {
        loggedIn: true,
      },
    };

    store = new Vuex.Store({
      modules: {
        myModule: {
          state,
        },
      },
    });

    wrapper = shallowMount(CvAbout, {
      store,
      localVue,
      methods: {
        checkAuth,
        downloadCV,
        saveCV,
        convertBuffer,
        simulateLoading,
        scrollTop,
      },
    });
  });

  test('renders correct contnent', () => {
    const email = wrapper.find('.email').text();
    const phone = wrapper.find('.phone').text();
    const location = wrapper.find('.location').text();
    const qualification = wrapper.find('.qualification').text();
    const buttonText = wrapper.find('.download-cv').text();
    expect(email).toStrictEqual('nickwintertaia@gmail.com');
    expect(phone).toStrictEqual('0274929272');
    expect(location).toStrictEqual('Auckland, New Zealand');
    expect(qualification).toStrictEqual('Bachelor\'s Degree in Computer and Information Sciences');
    expect(buttonText).toStrictEqual('Download CV');
  });

  test('Download CV function is called', async () => {
    const downloadButton = wrapper.find('.download-cv');
    await downloadButton.trigger('click');
    expect(downloadCV).toHaveBeenCalled();
  });
});
