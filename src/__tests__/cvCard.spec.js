import { shallowMount } from '@vue/test-utils';
import CvCard from '../components/cvCard.vue';

describe('cvCard.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(CvCard, {
      propsData: {
        cardTitle: 'MyCard',
        progress: 50,
      },
      slots: {
        default: '<p class="test-content">test content</p>',
      },
    });
  });

  test('Card component passes data correctly', () => {
    const slot = wrapper.find('.test-content').text();
    expect(wrapper.vm.cardTitle).toBe('MyCard');
    expect(wrapper.vm.progress).toStrictEqual(50);
    expect(slot).toStrictEqual('test content');
  });
});
