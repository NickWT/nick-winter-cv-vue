import { shallowMount, createLocalVue } from '@vue/test-utils';
import CvExperience from '../views/cvExperience.vue';
import Vuex from 'vuex';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('cvExperience.vue', () => {
  let wrapper;
  let checkAuth = jest.fn();
  let displayModal = jest.fn();
  let onModalHide = jest.fn();
  let simulateLoading = jest.fn();
  let scrollTop = jest.fn();

  let state;
  let store;

  beforeEach(() => {
    state = {
      is: {
        loggedIn: true,
      },
    };

    store = new Vuex.Store({
      modules: {
        myModule: {
          state,
        },
      },
    });

    wrapper = shallowMount(CvExperience, {
      store,
      localVue,
      methods: {
        checkAuth,
        displayModal,
        onModalHide,
        simulateLoading,
        scrollTop,
      },
    });
  });

  test('renders correct contnent', () => {
    const title = wrapper.find('.title').text();
    const job1 = wrapper.find('.job1').text();
    const job2 = wrapper.find('.job2').text();
    const job3 = wrapper.find('.job3').text();
    const job4 = wrapper.find('.job4').text();
    const job5 = wrapper.find('.job5').text();
    expect(title).toStrictEqual('Experience');
    expect(job1).toContain('Graduate Software Developer, Smartpay');
    expect(job2).toContain('Level 2 IT Support Technician, Smartpay');
    expect(job3).toContain('Intern Front-End Software Developer, Smartpay');
    expect(job4).toContain('Level 1 IT Support Technician, Smartpay');
    expect(job5).toContain('Sales assistant, Bunnings Warehouse Silverdale');
  });
});
