import { shallowMount } from '@vue/test-utils';
import CvLoader from '../components/cvLoader.vue';

test('Does render correctly', () => {
  const wrapper = shallowMount(CvLoader);
  expect(wrapper.html()).toMatchSnapshot();
});

test('Does bind the correct class', () => {
  const wrapper = shallowMount(CvLoader, {
    propsData: {
      type: 'text-sm',
    },
  });

  expect(wrapper.vm.type).toBe('text-sm');
});
