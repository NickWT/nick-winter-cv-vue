import { shallowMount } from '@vue/test-utils';
import CvModal from '../components/cvModal.vue';

test('Does render when show prop is true', () => {
  const wrapper = shallowMount(CvModal, {
    propsData: {
      show: true,
    },
  });
  expect(wrapper).not.toBeNull();
});

test('Does render correctly', () => {
  const wrapper = shallowMount(CvModal, {
    propsData: {
      show: true,
    },
  });
  expect(wrapper.html()).toMatchSnapshot();
});

test('Does render slot correctly', () => {
  const wrapper = shallowMount(CvModal, {
    slots: {
      default: '<p>test content</p>',
    },
    propsData: {
      show: true,
    },
  });
  expect(wrapper.html()).toMatchSnapshot();
});
