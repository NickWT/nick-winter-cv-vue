import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import CvNavTop from '../components/cvNavTop.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('CvNavTop.vue', () => {
  const logOut = jest.fn();
  const openDropdown = jest.fn();
  const closeDropdown = jest.fn();
  const simulateLoading = jest.fn();
  const setListener = jest.fn();
  let scrollTop = jest.fn();

  let state;
  let mutations;
  let store;
  let wrapper;

  beforeEach(() => {
    state = {
      is: {
        loggedIn: true,
      },
    };

    mutations = {
      setAuth: jest.fn(),
      authSignOut: jest.fn(),
    };

    store = new Vuex.Store({
      modules: {
        myModule: {
          state,
          mutations,
        },
      },
    });
    
    wrapper = shallowMount(CvNavTop, {
      stubs: ['router-link'],
      store,
      localVue,
      computed: {
        ...mapState({
          loggedIn: () => true,
        })
      },
      methods: {
        logOut,
        openDropdown,
        closeDropdown,
        simulateLoading,
        setListener,
        scrollTop
      },
    });
  });

  test('logOut function called when logout button clicked', async () => {
    const button = wrapper.find('button');
    await button.trigger('click');
    expect(logOut).toHaveBeenCalled();
  });

  test('openDropdown function called when dropdown button clicked', async () => {
    const button = wrapper.find('.dropbtn');
    await button.trigger('click');
    expect(openDropdown).toHaveBeenCalled();
  });
});
