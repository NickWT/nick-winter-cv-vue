import Vue from "vue"; // Include Vue
import Router from "vue-router"; // Include Vue Router libary
import Login from "./views/cvLogin.vue"
import About from "./views/cvAbout.vue";
import References from "./views/cvReferences.vue";
import Experience from './views/cvExperience';
import Skills from './views/cvSkills.vue';

Vue.use(Router);

export default new Router({
  mode: "history",
    routes: [
      {
        path: "/",
        name: "Login",
        component: Login,
      },
      {
        path: "/about",
        name: "About",
        component: About,
      },
      {
        path: "/references",
        name: "References",
        component: References,
      },
      {
        path: "/experience",
        name: "Experience",
        component: Experience,
      },
      {
        path: "/skills",
        name: "Skills",
        component: Skills,
      },
    ]
  });
